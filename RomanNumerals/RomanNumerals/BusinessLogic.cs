﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace RomanNumerals
{
    public class BusinessLogic
    {
        public string ConvertNumberToRomanNumeral(int numberToConvert)
        {
            var digits = numberToConvert.ToString().ToList();
            digits.Reverse();
            var result = string.Empty; //new System.Text.StringBuilder();

            if (digits.Count() > 0)
            {
                result = GetUnits(int.Parse(digits[0].ToString())) + result;//.Append(GetUnits(int.Parse(digits[0].ToString())));
            }

            if (digits.Count() > 1)
            {
                result = GetTens(int.Parse(digits[1].ToString())) + result;
            }

            //var test = result.ToString().ToList<char>();
            //test.Reverse();
            return result; 
        }    

        private string GetUnits(int digit)
        {
            if (digit == 1)
            {
                return "I";
            }
            else if (digit == 2)
            {
                return "II";
            }
            else if (digit == 3)
            {
                return "III";
            }
            else if (digit == 4)
            {
                return "IV";
            }
            else if (digit == 5)
            {
                return "V";
            }
            else if (digit == 6)
            {
                return "VI";
            }
            else if (digit == 7)
            {
                return "VII";
            }
            else if (digit == 8)
            {
                return "VIII";
            }
            else if (digit == 9)
            {
                return "IX";
            }
            else return "";
        }

        private string GetTens(int digit)
        {
            if (digit == 1)
            {
                return "X";
            }
            else if (digit == 2)
            {
                return "XX";
            }
            else if (digit == 3)
            {
                return "XXX";
            }
            else if (digit == 4)
            {
                return "XL";
            }
            else if (digit == 5)
            {
                return "L";
            }
            else if (digit == 6)
            {
                return "LX";
            }
            else if (digit == 7)
            {
                return "LXX";
            }
            else if (digit == 8)
            {
                return "LXXX";
            }
            else if (digit == 9)
            {
                return "XC";
            }
            else return "";
        }
    }
}