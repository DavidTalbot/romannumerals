﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RomanNumerals.Tests
{
    [TestClass]
    public class BusinessLogicTests
    {
      
        [TestMethod]
        public void given_number_return_roman()
        {
            var sut = new BusinessLogic();

            var TestCases = new Dictionary<int, string>();
            TestCases.Add(1, "I");
            TestCases.Add(2, "II");
            TestCases.Add(3, "III");
            TestCases.Add(4, "IV");
            TestCases.Add(5, "V");
            TestCases.Add(6, "VI");
            TestCases.Add(7, "VII");
            TestCases.Add(8, "VIII");
            TestCases.Add(9, "IX");
            TestCases.Add(10, "X");
            TestCases.Add(11, "XI");
            TestCases.Add(12, "XII");
            TestCases.Add(13, "XIII");
            TestCases.Add(14, "XIV");
            TestCases.Add(40, "XL");
            TestCases.Add(42, "XLII");
            TestCases.Add(99, "XCIX");


            foreach (var item in TestCases)
            {
                Assert.AreEqual(sut.ConvertNumberToRomanNumeral(item.Key), item.Value);
            }
          
        }

    }
}
